//
//  ViewController.m
//  recipe
//
//  Created by Mohammad Jebelli on ۲۰۱۶/۱/۳۰.
//  Copyright © ۲۰۱۶ cs3260. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end






@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //  Do any additional setup after loading the view, typically from a nib.
    
    
    self.title = @"Persian Cuisine Recipies";
    NSString* path = [[NSBundle mainBundle]pathForResource:@"PropertyList" ofType:@"plist"];
    
    array = [[NSArray alloc]initWithContentsOfFile:path];
    
    //array = @[@"Test"];
    
    
    
    myTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    [self.view addSubview:myTableView];
    
    //NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return array.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *) indexPath {
    
    
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell) {
    
        cell =[[UITableViewCell alloc]
               initWithStyle:
               UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    
    NSDictionary* dictionary = array[indexPath.row];
    
    
    
    NSString* stringToDisplay = dictionary[@"title"];
    
    
    cell.textLabel.text = stringToDisplay;
    
    return cell;
    
    
  
}









-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSDictionary* dictionary = array[indexPath.row];

    
    DetailViewController* dvc = [DetailViewController new];
    dvc.infoDictionary = dictionary;
    
    [self.navigationController pushViewController:dvc animated:YES];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
