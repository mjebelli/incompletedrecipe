//
//  ViewController.h
//  recipe
//
//  Created by Mohammad Jebelli on ۲۰۱۶/۱/۳۰.
//  Copyright © ۲۰۱۶ cs3260. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"


@interface ViewController : UIViewController<
UITableViewDataSource, UITableViewDelegate>
{
    UITableView* myTableView;
    NSArray* array;
}


@end

