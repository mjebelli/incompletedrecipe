//
//  AppDelegate.h
//  recipe
//
//  Created by Mohammad Jebelli on ۲۰۱۶/۱/۳۰.
//  Copyright © ۲۰۱۶ cs3260. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

