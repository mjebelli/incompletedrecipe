//
//  main.m
//  recipe
//
//  Created by Mohammad Jebelli on ۲۰۱۶/۱/۳۰.
//  Copyright © ۲۰۱۶ cs3260. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
