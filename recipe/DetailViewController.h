//
//  DetailViewController.h
//  recipe
//
//  Created by Mohammad Jebelli on ۲۰۱۶/۲/۴.
//  Copyright © ۲۰۱۶ cs3260. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (nonatomic, strong) NSDictionary* infoDictionary;


 
@end
